import { BASE_URL } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable ,throwError, BehaviorSubject} from 'rxjs';
import { Router } from '@angular/router';


const BASE_API =BASE_URL+'/api/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(
    private http: HttpClient , 
    private router: Router,
    ) { }


  find_airlines( airport:string ,airportD:string): Observable<any> {
    return this.http.post(BASE_API + 'find_airlines', {
      airport,
      airportD
    }, httpOptions);
  }
  get_airports(country: string): Observable<any> {
    return this.http.get<any[]>(`${BASE_API}get_airports/${country}`)
  }

  rate (email:string,airport:string,airportD:string,airline:string,rating:number): Observable<any> {
    return this.http.post(BASE_API + 'rate', {
      email,
      airport,
      airportD,
      airline,
      rating
    }, httpOptions);
  }

 
 }

