import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { UserRoutingModule  } from './user-routing.module';
import { FormComponent } from './components/form/form.component';
import { ContainerComponent } from './components/container/container.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    FormComponent,
    ContainerComponent,

  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule

   
  ],
  providers: [
   
  ]
})
export class UserModule { }
