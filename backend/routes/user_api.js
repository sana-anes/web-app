const express = require('express');
const router = express.Router();
const neo4j_calls = require('../neo4j_calls/neo4j_api');

router.get("/get_airports/:country", async (req, res) => {
    const {country}=req.params;
    let result = await neo4j_calls.get_airports(country);
    res.status(200).send(result)

});

router.post("/find_airlines", async (req, res) =>{
    console.log(req.body)
    const{airport,airportD}=req.body;
    let result = await neo4j_calls.find_airlines(airport,airportD);
    res.status(200).send(result)
});
router.post("/rate", async (req, res) =>{
    console.log(req.body)
    const{email,airport,airportD,airline,rating}=req.body;
    let user = await neo4j_calls.rating(email,airport,airportD,airline,rating);
    res.send(user)
});

module.exports = router;