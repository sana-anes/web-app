let neo4j = require('neo4j-driver');
let { creds } = require("./../config/credentials");
let driver = neo4j.driver("bolt://0.0.0.0:7687", neo4j.auth.basic(creds.neo4jusername, creds.neo4jpw));

Top_Airlines=async function (){
   
    const spawn = require('child_process').spawn;
    const process=spawn('python',['./hello.py']);
    process.stdout.on('data',data =>{
        console.log("here")
        console.log(data.toString())
    return data.toString();
});
}
let List_Final= ['Southwest','Delta','United','American' ,'US Airways' ,'Virgin America'];


exports.get_airports = async function (country) {
    let session = driver.session();
    const airports = await session.run('MATCH (n:Airport {country:$country}) RETURN n.name ', {country:country});
    session.close();
    return (airports);
};

exports.find_airlines = async function (airport, airportD) {

    let session = driver.session();    
    const airlines = await session.run(`MATCH (r:Route) - [rby:by] ->(a:Airline) 
                                        WHERE EXISTS {  MATCH (r) - [rfrom:from]-> (a1:Airport {name: $airport})
                                            WHERE EXISTS {
                                                MATCH (r) - [rto:to]-> (a2:Airport {name: $airportD})
                                            }
                                        }
                                        return DISTINCT a.name`, {
       airport:airport,
       airportD:airportD,

   });
    session.close();
    let air = airlines.records.filter(x => List_Final.includes(x._fields[0]));
    console.log(air.length)
    if (air.length==0){
        return (airlines);
        
    }
    else {
        return(air);
    }

 
};

exports.rating = async function (email,airport,airportD,airline,rating) {
    let session = driver.session();
    let user = "No Rate Was Created";
    try {
         user = await session.run(
            `MERGE (n:Rate {email:$email, airport_source:$airport,airport_destination:$airportD,rating:$rating}) 
            CREATE (a:Airline {name:$name})-[:Have]->(n)                        
             return n ` 
        , { email:email,
            airport:airport,
            airportD:airportD,
            rating:rating,
            name: airline,

        });
    }
    catch (err) {
        console.error(err);
        return user;
    }
    return user;
}

