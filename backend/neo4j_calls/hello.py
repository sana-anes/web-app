import pandas as pd
import numpy as np
tweets=pd.read_csv('data/Airline-Sentiment-2-w-AA.csv',encoding = "ISO-8859-1")
is_positive = tweets['airline_sentiment'].str.contains("positive")
is_negative = tweets['airline_sentiment'].str.contains("negative")
is_neutral = tweets['airline_sentiment'].str.contains("neutral")
positive_tweets = tweets[is_positive]
neutral_tweets = tweets[is_neutral]
# Create the rank for the best airline
best_airline = positive_tweets[['airline','airline_sentiment:confidence']]
cnt_best_airline = best_airline.groupby('airline', as_index=False).count()
Top_airline=cnt_best_airline.sort_values('airline_sentiment:confidence', ascending=False)
Top=np.array(Top_airline['airline'])
print(Top)
